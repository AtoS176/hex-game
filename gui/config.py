class Configuration:
    width = 1440
    height = 900
    start_x = 250
    start_y = 200
    edge_number = 6
    field_angle = 30
    fields_per_row = 11
    fields_per_column = 11
    field_color = (255, 255, 255)
    field_radius = 25
    row_distance = 51
    column_distance = 58
    field_distance = 28
    person_color = (255, 0, 0)
    computer_color = (0, 255, 0)
