from enum import Enum
from gui.config import Configuration


class Player(Enum):
    COMPUTER = Configuration.computer_color
    PERSON = Configuration.person_color
    EMPTY = Configuration.field_color

    def is_empty(self):
        return self == Player.EMPTY
