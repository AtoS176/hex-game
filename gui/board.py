from gui.field import Field
from gui.config import Configuration
from gui.player import Player


class Board:
    def __init__(self, start_x, start_y, surface):
        self.x = start_x
        self.y = start_y
        self.surface = surface
        self.row_distance = Configuration.row_distance
        self.col_distance = Configuration.column_distance
        self.field_distance = Configuration.field_distance
        self.fields_per_col = Configuration.fields_per_column
        self.fields_per_row = Configuration.fields_per_row
        self.fields = self.__init_fields()

    def __init_fields(self):
        fields = list()

        for row in range(0, self.fields_per_row):
            for col in range(0, self.fields_per_col):
                fields.append(self.create_field(col, row))

        return fields

    def create_field(self, col, row):
        y = self.y + row * self.row_distance
        x = self.x + col * self.col_distance + row * self.field_distance
        field = Field(x, y, self.surface)
        field.draw()
        return field

    def make_move(self, positon, player: Player):
        for field in self.fields:
            if field.is_inside(positon[0], positon[1]):
                return field.change_color(player)
        return False
