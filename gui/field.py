import pygame
from gui.config import Configuration
from gui.player import Player
from math import cos
from math import sin


class Field:
    def __init__(self, x, y, surface):
        self.x = x
        self.y = y
        self.surface = surface
        self.edges = Configuration.edge_number
        self.radius = Configuration.field_radius
        self.angle = Configuration.field_angle
        self.player = Player.EMPTY
        self.points = list()

    def draw(self):
        pi2 = 2 * 3.14
        x, y = self.x, self.y

        for i in range(0, self.edges):
            angle = self.angle + pi2 * i / self.edges
            x = x + self.radius * cos(angle)
            y = y + self.radius * sin(angle)
            self.points.append([int(x), int(y)])

        pygame.draw.polygon(self.surface, self.player.value, self.points)

    def is_inside(self, a, b):
        return (self.x - a)**2 + (self.y - b)**2 < self.radius**2

    def change_color(self, player: Player):
        if self.player.is_empty():
            pygame.draw.polygon(self.surface, player.value, self.points)
            self.player = player
            return True
        return False
