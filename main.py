import pygame
from gui.board import Board
from gui.config import Configuration
from gui.player import Player


def run():
    scr = pygame.display.set_mode((Configuration.width, Configuration.height))
    board = Board(Configuration.start_x, Configuration.start_y, scr)
    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.MOUSEBUTTONUP:
                pos = pygame.mouse.get_pos()
                board.make_move(pos, Player.PERSON)
        pygame.display.flip()
    pygame.quit()


run()
